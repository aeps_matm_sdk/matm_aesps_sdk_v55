package com.matm.matmsdk.aepsmodule.transactionstatus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.matm.matmsdk.ChooseCard.ChooseCardActivity;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import isumatm.androidsdk.equitas.R;

public class TransactionStatusNewActivity extends AppCompatActivity {

    private RelativeLayout ll_maiin;
    private TextView statusMsgTxt,statusDescTxt;
    private ImageView status_icon;
    private TextView date_time,rref_num,aadhar_number,bank_name,card_amount,card_transaction_type,card_transaction_amount;
    public EasyLinkSdkManager manager;
    private Button backBtn;
    ChooseCardActivity chooseCardActivity;
    LinearLayout ll13,ll12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        ll_maiin = findViewById(R.id.ll_maiin);
       // session = new Session(TransactionStatusActivity.this);

        ll13 = findViewById(R.id.ll13);
        ll12 = findViewById(R.id.ll12);



        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        aadhar_number = findViewById(R.id.aadhar_number);
        rref_num = findViewById(R.id.rref_num);
        bank_name = findViewById(R.id.bank_name);
        card_transaction_type = findViewById(R.id.card_transaction_type);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        date_time = findViewById(R.id.date_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        if(getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY) == null){
            ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            //statusDescTxt.setText("UnFortunatly Paymet was rejected.");
            backBtn.setBackgroundResource(R.drawable.button_backgroundtransaction_fail);

        }else{
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus ().trim ().equalsIgnoreCase ( "0" )) {

                String aadharCard = transactionStatusModel.getAadharCard ();
                if (transactionStatusModel.getAadharCard () == null) {
                    aadharCard = "N/A";
                } else {
                    if(transactionStatusModel.getAadharCard ().equalsIgnoreCase ( "" )){
                        aadharCard = "N/A";
                    }else {
                        StringBuffer buf = new StringBuffer( aadharCard );
                        buf.replace ( 0, 10, "XXXX-XXXX-" );
                        System.out.println ( buf.length () );
                        aadharCard = buf.toString ();
                    }
                }

                String bankName = "N/A";
                if (transactionStatusModel.getBankName () != null && !transactionStatusModel.getBankName ().matches ( "" )) {
                    bankName = transactionStatusModel.getBankName ();
                }
                String referenceNo = "N/A";
                if (transactionStatusModel.getReferenceNo () != null && !transactionStatusModel.getReferenceNo ().matches ( "" )) {
                    referenceNo = transactionStatusModel.getReferenceNo ();
                }
                String balance = "N/A";
                if (transactionStatusModel.getBalanceAmount () != null && !transactionStatusModel.getBalanceAmount ().matches ( "" )) {
                    balance = transactionStatusModel.getBalanceAmount ();
                    if (balance.contains ( ":" )) {
                        String[] separated = balance.split ( ":" );
                        balance = separated[ 1 ].trim ();
                    }
                }
                String amount = "N/A";
                if (transactionStatusModel.getTransactionAmount () != null && !transactionStatusModel.getTransactionAmount ().matches ( "" )) {
                    amount = transactionStatusModel.getTransactionAmount ();
                }



                if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Cash Withdrawal" )) {
                    //detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount );

                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                }else if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Balance Enquery" )){
//                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance );

                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                }
            }else{
//                failureLayout.setVisibility(View.VISIBLE);
//                successLayout.setVisibility(View.GONE);
                statusMsgTxt .setText (transactionStatusModel.getApiComment ());
                statusDescTxt .setText (transactionStatusModel.getStatusDesc () );
            }
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
